import React, { useState } from 'react';
import InputComponent from '../../../components/Input';
import ButtonComponent from '../../../components/Button';
import AlertComponent from '../../../components/Alert';

const App: React.FunctionComponent = () => {
    const [inputValue, setInputValue] = useState<string>('');
    const [buttonDisabled, setButtonDisabled] = useState<boolean>(true);
    const [showAlert, setShowAlert] = useState<boolean>(false);

    const handleInputChange = (value: string) => {
        setInputValue(value);
        setButtonDisabled(value === '');
        setShowAlert(false);
    };

    const handleButtonClick = () => {
        setShowAlert(true);
    };

    const handleAlertClose = () => {
        setShowAlert(false);
    };

    return (
        <div style={{display:'flex',flexDirection:'column',gap:'2rem',marginTop:'1rem', justifyContent:'center',alignItems:'center'}}>
            <InputComponent
                value={inputValue}
                onChange={handleInputChange}
                placeholder="Enter a value"
            />
            <ButtonComponent
                onClick={handleButtonClick}
                disabled={buttonDisabled}
            />
            {showAlert && (
                <AlertComponent
                    message="Alert!"
                    onClose={handleAlertClose}
                />
            )}
        </div>
    );
};

export default App;
