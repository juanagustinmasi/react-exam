import React, { lazy } from 'react';
import logo from './logo.svg';
import './App.css';
import { Route, Routes } from 'react-router-dom';
const Home = lazy(() => import('./pages/public/Home'));
function App() {
  return (

    <Routes>
      <Route path='/' element={<Home />} ></Route>
      <Route path='home' element={<Home />} ></Route>
    </Routes>
  );
}

export default App;
