import React from 'react';

interface ButtonProps {
    onClick: () => void;
    disabled: boolean;
}

const ButtonComponent = ({ onClick, disabled }: ButtonProps) => {
    return (
        <button onClick={onClick} disabled={disabled}>
            Show Alert
        </button>
    );
};

export default ButtonComponent;
