import React from 'react';

interface AlertProps {
    message: string;
    onClose: () => void;
}

const AlertComponent = ({ message, onClose }: AlertProps) => {
    return (
        <div className="alert" style={{ display: 'flex',margin:'2rem' ,border:'1px  black solid' }}>
            <span className="close" onClick={onClose}>
                <svg style={{ display: 'flex' ,justifyContent: 'left',border:'1px  black solid' }} xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24">
                    <path fill="none" d="M0 0h24v24H0V0z" />
                    <path d="M18.3 5.71a.996.996 0 0 0-1.41 0L12 10.59 7.11 5.7A.996.996 0 1 0 5.7 7.11L10.59 12 5.7 16.89a.996.996 0 1 0 1.41 1.41L12 13.41l4.89 4.89a.996.996 0 1 0 1.41-1.41L13.41 12l4.89-4.89c.38-.38.38-1.02 0-1.4z" />
                </svg>
            </span>
            {message}
        </div>
    );
};

export default AlertComponent;
