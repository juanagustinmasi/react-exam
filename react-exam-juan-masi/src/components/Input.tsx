import React from 'react';

interface InputProps {
    value: string;
    onChange: (value: string) => void;
    placeholder: string;
}

const InputComponent = ({ value, onChange, placeholder }: InputProps) => {
    const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        onChange(e.target.value);
    };

    return (
        <input
            type="text"
            value={value}
            onChange={handleChange}
            placeholder={placeholder}
        />
    );
};

export default InputComponent;
